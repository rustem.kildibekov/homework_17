// Homework.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <string>
#include <cmath>

using std::string;



class Values {
    

public:
    Values() : _boolValue(false), _intValue(0), _doubleValue(0), _stringValue("") {
    }
    
    Values(bool boolValue, int intValue, double doubleValue, string stringValue) {
        setBoolValue(boolValue);
        setIntValue(intValue);
        setDoubleValue(doubleValue);
        setStringValue(stringValue);
    }

    void setBoolValue(bool value) {_boolValue = value; };
    void setIntValue(int value) { _intValue = value; };
    void setDoubleValue(double value) { _doubleValue = value; };
    void setStringValue(string value) { _stringValue = value; };

    bool getBoolValue() { return _boolValue; };
    int  getIntValue() { return _intValue; };
    double getDoubleValue() { return _doubleValue; };
    string getStringValue() { return _stringValue; };

    void showState() {
        std::cout << "boolValue = " << getBoolValue() << ", ";
        std::cout << "intValue = " << getIntValue() << ", ";
        std::cout << "doubleValue = " << getDoubleValue() << ", ";
        std::cout << "stringValue = " << getStringValue() << "\n";
    }

private:
    bool _boolValue;
    int _intValue;
    double _doubleValue;
    string _stringValue; 
};

class Vector {
public:
    Vector() {
    }
    Vector(double x, double y, double z) : _x(x), _y(y), _z(z){
    }

    void show() {
        std::cout << "Vector (" << _x << ", " << _y << ", " << _z << ")\n";
        std::cout << "Lenght = " << getLenght() << "\n";
    }
    double getLenght() {
        return abs(sqrt(pow(_x, 2) + pow(_y, 2) + pow(_z, 2)));
    }

private:    
    double _x = 0;
    double _y = 0;
    double _z = 0;
};




int main()
{
    std::cout << "Init 'Values' class use constructor without parameters \n";
    Values v1;
    v1.showState();

    std::cout << "\nEdit variables \n";
    v1.setBoolValue(true);
    v1.setIntValue(12345);
    v1.setDoubleValue(123.0456);
    v1.setStringValue("test");
    v1.showState();

    std::cout << "\nInit 'Values' class use constructor with parameters \n";
    Values v2(true, 54321, 321.0654, "test2");
    v2.showState();

    std::cout << "\nCreate vector \n";
    Vector vec1(5, 10, 12.6);
    vec1.show();

}


